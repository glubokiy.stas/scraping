import scrapy

from scrapers.artlebedev.items import ArtlebedevLynch


class ArtlebedevSpider(scrapy.Spider):
    name = 'artlebedev'
    allowed_domains = ['artlebedev.ru']
    start_urls = ['https://www.artlebedev.ru/kovodstvo/business-lynch/']

    custom_settings = {
        'MONGODB_COLLECTION': 'artlebedev_business_lynch',
        'MONGODB_DEDUP_ENABLED': True
    }

    def parse(self, response):
        for a_tag in response.css('.everyday-month-calendar-all a.title'):
            yield response.follow(a_tag, self.parse_year)

    def parse_year(self, response):
        for a_tag in response.css('.everyday-month-calendar a'):
            yield response.follow(a_tag, self.parse_lynch)

    def parse_lynch(self, response):
        date = response.url[-11:-1]
        self.logger.info(f'Scraping {date}')

        item = ArtlebedevLynch()

        item['date'] = date
        item['url'] = response.url
        item['q_name'] = response.css('.business-lynch-top-info .item:nth-child(1) > .value::text').get()
        item['q_text'] = ''.join(
            response.css('.business-lynch-top-info .item:nth-child(2) > .value').xpath('.//text()').getall()
        )
        item['image'] = response.urljoin(response.css('#Lynch > img::attr(src)').get())
        item['a_name'] = response.css('.business-lynch-top-info .item.author .name > a::text').get()
        item['a_comments'] = [
            ' '.join(a.strip() for a in x.css('::text').getall())
            for x in response.css('.LynchComment > div')
        ]

        yield item
