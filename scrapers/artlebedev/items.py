from scrapy import Field, Item

from utils import replace_nbsp


class ArtlebedevLynch(Item):
    date = Field()
    url = Field()
    q_name = Field(serializer=replace_nbsp)
    q_text = Field(serializer=replace_nbsp)
    image = Field()
    a_name = Field(serializer=replace_nbsp)
    a_comments = Field(serializer=replace_nbsp)
