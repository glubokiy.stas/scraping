from random import sample, shuffle

from scrapy import Spider
from scrapy_splash import SplashRequest

from scrapers.settings import UA_LIST
from scrapers.subway.constants import COORDINATES_EXTRACTION_SCRIPT, HEADERS
from utils.deg2dec import deg2dec


class SubwaySpider(Spider):
    name = 'subway'
    start_urls = ['https://en.wikipedia.org/wiki/List_of_metro_systems']

    custom_settings = {
        'MONGODB_COLLECTION': 'subway_coords',
        'MONGODB_DEDUP_ENABLED': True,
        'CONCURRENT_REQUESTS': 1,
        'AUTOTHROTTLE_ENABLED': True,
        'EXTENSIONS': {
            'scrapy.extensions.throttle.AutoThrottle': None,
            'scrapers.extensions.AutoThrottle': 0
        }
    }

    def parse(self, response):
        """Parse subway systems list form Wikipedia."""
        table = response.xpath('//h2[contains(., "List")]/following-sibling::table[1]')

        city_requests = []
        for row in table.css('tbody > tr'):
            cols = row.css('td')
            num_cols = len(cols)

            if num_cols == 8:
                city_cell = cols[1]
            elif num_cols == 7:
                city_cell = cols[0]
            else:
                continue

            city_name = city_cell.css('a::text').get()
            city_link = city_cell.css('a::attr(href)').get()

            city_requests.append(response.follow(city_link, self.parse_city, meta={'city': city_name}))

        shuffle(city_requests)
        return city_requests

    def parse_city(self, response):
        """Parse city page on Wikipedia to get its coordinates."""
        lat_deg = response.css('.geo-default .latitude::text').get()
        lon_deg = response.css('.geo-default .longitude::text').get()

        if lat_deg is None:
            lat_deg, lon_deg = response.css('.geo-dec::text').get().split()

        lat = deg2dec(lat_deg)
        lon = deg2dec(lon_deg)

        url = f'http://wikimapia.org/#lang=en&lat={lat}&lon={lon}&z=11&m=w&tag=44758'

        headers = HEADERS.copy()
        headers['user-agent'] = sample(UA_LIST, 1)[0]

        yield SplashRequest(
            url,
            self.parse_map,
            endpoint='execute',
            args={
                'lua_source': COORDINATES_EXTRACTION_SCRIPT,
                'headers': headers
            },
            meta=response.meta
        )

    def parse_map(self, response):
        """Parse map."""
        markers = response.data['markers'].values()

        self.logger.info(f'EXPORTING {len(markers)} MARKERS FROM {response.meta["city"]}')

        for marker in markers:
            yield {**marker, 'city': response.meta['city']}
