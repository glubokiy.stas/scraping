COORDINATES_EXTRACTION_SCRIPT = """
function main(splash)
    assert(splash:go(splash.args.url))
    splash:wait(0.5)
    local num_markers = splash:evaljs("app.map.markersLayer.features.length")

    local markers = {}
    for i=1,num_markers do
        local marker_options = "app.map.markersLayer.features[" .. i-1 .. "].options."
        local lat = splash:evaljs(marker_options .. "coords.lat")
        local lon = splash:evaljs(marker_options .. "coords.lng")
        local titles = splash:evaljs(marker_options .. "obj.titles")
        markers[i] = {lat=lat, lon=lon, titles=titles}
    end

    return {markers=markers}
end
"""

HEADERS = {
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
    "accept-language": "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7",
    "cache-control": "no-cache",
    "pragma": "no-cache",
    "upgrade-insecure-requests": "1",
    "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
}
