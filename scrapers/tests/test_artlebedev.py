import os

from scrapers.artlebedev.crawler import ArtlebedevSpider
from test_utils import unittest


class ArtlebedevParseLynchSpiderTest(unittest.SpiderCallbackTestCase):

    callback = ArtlebedevSpider.parse_lynch
    response_html = os.path.join(os.path.dirname(__file__), 'responses', 'artlebedev.html')
    url = 'https://www.artlebedev.ru/kovodstvo/business-lynch/2018/08/07/'

    def check_results(self, results):
        self.assertEqual(1, len(results))

        item = results[0]
        self.assertEqual(4, len(item['a_comments']))
        self.assertEqual('Таисия Лушенко', item['a_name'])
        self.assertEqual('Человек-завод', item['q_name'])
        self.assertEqual('2018/08/07', item['date'])
        self.assertIn('Вдохновился ломаными массивными конструкциями музея-завода Куйбышева', item['q_text'])
        self.assertIn('Интересные формы получились.', item['a_comments'][0])
        self.assertEqual(
            'https://img.artlebedev.ru/kovodstvo/business-lynch/files/C/3/C3CE73B3-07EE-4488-B5F8-C2E67BAFC74E.png',
            item['image']
        )
