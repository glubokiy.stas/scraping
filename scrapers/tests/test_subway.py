import os

from scrapy import Request
from scrapy.http import Response
from scrapy_splash import SplashRequest

from scrapers.subway.constants import COORDINATES_EXTRACTION_SCRIPT
from scrapers.subway.crawler import SubwaySpider
from test_utils import unittest


class SubwayParseSpiderTest(unittest.SpiderCallbackTestCase):
    callback = SubwaySpider.parse
    url = 'https://en.wikipedia.org/wiki/List_of_metro_systems'
    response_html = os.path.join(os.path.dirname(__file__), 'responses', 'wiki_subway.html')

    def check_results(self, requests):
        cities = [request.meta.get('city') for request in requests]

        self.assertGreater(len(cities), 150)
        self.assertIn('Algiers', cities)
        self.assertIn('Fukuoka', cities)
        self.assertIn('Hiroshima', cities)
        self.assertIn('Kobe', cities)
        self.assertIn('Caracas', cities)


class SubwayParseMapSpiderTest(unittest.SpiderCallbackTestCase):
    callback = SubwaySpider.parse_map
    url = 'http://wikimapia.org/#lang=en&lat=53.901102&lon=27.566414&z=12&m=w&tag=44758'
    meta = {'city': 'Minsk'}

    request_cls = SplashRequest
    request_args = {
        'endpoint': 'execute',
        'args': {'lua_source': COORDINATES_EXTRACTION_SCRIPT},
        'meta': meta
    }

    def check_results_local(self, results):
        self.assertEqual(2, len(results))

        item = results[0]
        self.assertEqual(1, item['lat'])
        self.assertEqual(2, item['lon'])
        self.assertEqual({'2': 'Station 1'}, item['titles'])

    def check_results_remote(self, results):
        self.assertGreater(len(results), 5)

        item = results[0]
        self.assertTrue(item['lon'])
        self.assertTrue(item['lat'])
        self.assertTrue(item['titles'])

    def test_callback_local(self):
        response = Response(self.url, request=Request(self.url, meta=self.meta))
        response.data = {
            'markers': {
                '1': {
                    'lat': 1,
                    'lon': 2,
                    'titles': {
                        '2': 'Station 1'
                    }
                },
                '2': {
                    'lat': 3,
                    'lon': 4,
                    'titles': {
                        '2': 'Station 2'
                    }
                }
            }
        }

        callback = self.get_callback()
        results = list(callback(self.spider, response))

        self.check_results_local(results)
