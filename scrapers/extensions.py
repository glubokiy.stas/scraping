import logging

from scrapy.extensions.throttle import AutoThrottle as ScrapyAT

logger = logging.getLogger(__name__)


class AutoThrottle(ScrapyAT):

    def _adjust_delay(self, slot, latency, response):
        if '/execute' in response.url:
            logger.info('wikimapia in response, setting slot delay')
            slot.delay = 5.0
        else:
            slot.delay = 0.1
