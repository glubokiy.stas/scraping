from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError
from scrapy import Item
from scrapy.crawler import Crawler
from scrapy.settings import Settings
from scrapy.statscollectors import StatsCollector


class MongoDBPipeline(object):

    def __init__(self, settings: Settings, stats: StatsCollector):
        self.stats = stats

        if not settings['MONGODB_SERVER']:
            self.is_connected = False
            return

        connection = MongoClient('mongodb://{username}:{password}@{server}'.format(
            username=settings['MONGODB_USERNAME'],
            password=settings['MONGODB_PASSWORD'],
            server=settings['MONGODB_SERVER'],
            serverSelectionTimeoutMS=5000
        ))

        db = connection[settings['MONGODB_DB']]

        try:
            db.command('ping')
        except ServerSelectionTimeoutError:
            self.is_connected = False
        else:
            self.is_connected = True

        self.dedup_enabled = settings.getbool('MONGODB_DEDUP_ENABLED')
        self.collection = db[settings.get('MONGODB_COLLECTION', 'default')]

    def process_item(self, item: Item, _):
        if self.is_connected:
            dict_item = dict(item)

            if self.dedup_enabled:
                to_insert = self.collection.find_one(dict_item) is None
            else:
                to_insert = True

            if to_insert:
                self.collection.insert(dict_item)
                self.stats.inc_value('items_mongodb_inserted')
            else:
                self.stats.inc_value('items_mongodb_skipped_dupe')

        return item

    @classmethod
    def from_crawler(cls, crawler: Crawler):
        return cls(crawler.settings, crawler.stats)
