import base64
import io
import logging
import os
from datetime import datetime
from os import environ
from os.path import join, dirname

import humanize
from flask import Flask, render_template, jsonify, request, flash, redirect
from flask_httpauth import HTTPBasicAuth
from pymongo.collection import Collection

from artlebedev_analyzer import get_swear_documents
from pace_analyzer import get_graph
from exchange_rates import update_and_notify, get_user
from mongo import mongo

app = Flask(
    __name__,
    template_folder=join(dirname(__file__), 'templates'),
    subdomain_matching=True
)
app.config['SERVER_NAME'] = environ.get('SERVER_NAME')
app.secret_key = '12345678'  # not important since only used for flashed images
auth = HTTPBasicAuth()

if environ.get('FLASK_ENV') != 'development':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
else:
    app.config['TEMPLATES_AUTO_RELOAD'] = True

Cache = {
    'subway': {},
    'artlebedev': {}
}

PER_PAGE = 6


@app.route('/', subdomain='subway')
def subway_home():
    col = mongo.scraping.subway_coords
    doc_num = col.count_documents({})

    if doc_num not in Cache['subway']:
        data = col.find()

        coords = [{
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': [row['lon'], row['lat']]
            },
            'properties': {
                'city': row['city'],
            }
        } for row in data]

        Cache['subway'][doc_num] = render_template('subway.mapbox.html', coords=coords)

        app.logger.info('Doc number has changed. Updating cache...')

    return Cache['subway'][doc_num]


@app.route('/', subdomain='artlebedev')
@app.route('/<int:page>', subdomain='artlebedev')
def artlebedev_home(page=1):
    col = mongo.scraping.artlebedev_business_lynch
    doc_num = col.count_documents({})

    if doc_num not in Cache['artlebedev']:
        data = col.find()
        lynches = get_swear_documents(data)

        Cache['artlebedev'][doc_num] = lynches
        lynches.sort(key=lambda x: x['date'], reverse=True)

        app.logger.info('Doc number has changed. Updating cache...')

    lynches = Cache['artlebedev'][doc_num]
    start_idx = PER_PAGE * (page - 1)
    end_idx = PER_PAGE * page
    has_next_page = end_idx < len(lynches)

    lynches = lynches[start_idx:end_idx]

    return render_template('artlebedev.html', lynches=lynches, page=page, has_next_page=has_next_page)


@auth.verify_password
def verify_password(username, password):
    if username == os.getenv('EXCHANGE_USER') and password == os.getenv('EXCHANGE_PSWD'):
        return username
    return None


@app.route('/', subdomain='exchange')
@auth.login_required
def exchange_home():
    col = mongo.scraping.exchange_rates
    data = col.find_one({}, sort=[('at', -1)])

    alfa_rate = data['private']
    prior_rate = data['business']
    last_updated = data['at']
    _t = humanize.i18n.activate("ru_RU")

    return render_template(
        'exchange.html',
        alfa_rate=alfa_rate,
        prior_rate=prior_rate,
        last_updated_text=humanize.naturaltime(datetime.utcnow() - last_updated),
        user=get_user(),
    )


@app.route('/update-rates', subdomain='exchange', methods=['POST'])
@auth.login_required
def update_rates():
    is_ok = update_and_notify()
    return jsonify({'is_ok': is_ok})


@app.route('/toggle-notifications', subdomain='exchange', methods=['POST'])
@auth.login_required
def toggle_notifications():
    user = get_user()
    new_val = not user['notifications_enabled']
    col: Collection = mongo.scraping.users
    col.update_one({'username': user['username']}, {'$set': {'notifications_enabled': new_val}})
    return jsonify({'is_ok': True, 'data': {'notifications_enabled': new_val}})


@app.route('/', subdomain='gpx', methods=['GET', 'POST'])
def gpx_home():
    graph = None
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        splits = [int(x) for x in request.form['splits'].split(',')]

        graph = get_graph(file, splits)

        img = io.BytesIO()
        graph.savefig(img, format='png')
        img.seek(0)
        plot_url = base64.b64encode(img.getvalue()).decode()

        graph = '<img src="data:image/png;base64,{}">'.format(plot_url)
    return render_template('gpx.html', graph=graph)
