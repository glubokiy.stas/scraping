Flask==1.0.2
gunicorn==19.9.0
pymongo==3.8.0
nltk==3.4.4
humanize==3.11.0
Flask-HTTPAuth==4.4.0
pyTelegramBotAPI~=3.7.6
requests~=2.25.1
telegram~=0.0.1
parsel~=1.6.0

geopy~=2.2.0
gpxpy~=1.5.0
matplotlib~=3.0
numpy~=1.0
pandas~=1.0
