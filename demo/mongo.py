from os import getenv

from pymongo import MongoClient

mongo = MongoClient('mongodb://{username}:{password}@{server}'.format(
    username=getenv('MONGODB_USERNAME'),
    password=getenv('MONGODB_PASSWORD'),
    server=getenv('MONGODB_SERVER'),
    serverSelectionTimeoutMS=5000
))
