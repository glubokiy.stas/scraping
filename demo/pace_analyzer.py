import sys

import geopy.distance
import gpxpy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


def get_graph(file, splits):
    gpx = gpxpy.parse(file)
    data = []
    for point in gpx.tracks[0].segments[0].points:
        data.append(dict(latitude=point.latitude, longitude=point.longitude, time=point.time))
    df = pd.DataFrame(data)
    SHIFT_ROWS = 4
    SPLIT_BY_METERS = splits[0]
    # %%
    distances = []
    for (idx1, row1), (idx2, row2) in zip(df.iterrows(), df.shift(SHIFT_ROWS).iterrows()):
        try:
            distances.append(geopy.distance.distance([row1.latitude, row1.longitude], [row2.latitude, row2.longitude]).meters)
        except Exception as e:
            distances.append(None)
    # %%
    deltas = []
    for (idx1, row1), (idx2, row2) in zip(df.iterrows(), df.shift(SHIFT_ROWS).iterrows()):
        try:
            deltas.append((row1.time - row2.time).total_seconds())
        except Exception as e:
            deltas.append(None)
    # %%
    pace_df = pd.DataFrame({'dist': distances, 'time': deltas}).dropna() / SHIFT_ROWS
    # %%
    total_dist = pace_df.dist.sum()

    pace_df['cum_dist'] = pace_df.dist.cumsum()
    pace_df['cum_time'] = pace_df.time.cumsum()
    # %%
    floored_dist = np.floor(total_dist / SPLIT_BY_METERS) * SPLIT_BY_METERS
    # %%
    pace_df = pace_df[pace_df['cum_dist'] < floored_dist].copy()
    # %%
    pace_df['bin'] = pd.cut(pace_df.cum_dist, bins=list(range(0, int(total_dist), SPLIT_BY_METERS)))
    # %%
    gb = pace_df.groupby('bin')
    split_times = np.array([gb.get_group(interval).time.sum() for interval in gb.groups])
    speeds = SPLIT_BY_METERS / split_times

    # %%
    def get_pace_label(p):
        mins, secs = divmod(p, 60)
        return f'{int(mins)}"{int(secs):02d}\''

    # %%
    avg_speed = pace_df.dist.sum() / pace_df.time.sum()
    # %%
    width_pct = 0.95
    min_speed = speeds.min()
    max_speed = speeds.max()

    fig, ax = plt.subplots()
    x = np.array([interval.left for interval in gb.groups])

    # Draw bars
    ax.bar(x, height=speeds, width=SPLIT_BY_METERS * width_pct, align='edge')

    # Change xticks
    xticks = range(0, int(total_dist), SPLIT_BY_METERS)
    ax.set_xticks(xticks, minor=True)
    # ax.set_xticklabels(xticks, minor=True)
    ax.tick_params('x', which='minor', labelcolor='#666')
    ax.set_xticks(xticks[::3])
    ax.tick_params('x', labelrotation=-45, which='both', labelsize=9)

    ax.set_xlabel('Distance (m)')

    # Change yticks
    paces = np.arange(60, 600, 10)
    ax.set_yticks([1000 / p for p in paces])
    ax.set_yticklabels([get_pace_label(p) for p in paces])
    ax.tick_params('y', labelsize=9)

    ax.set_ylabel('Pace (mins/km)')

    # Change ylimit
    ax.set_ylim(min_speed * 0.95, max_speed * 1.05)

    # Adding pace lines
    ax.grid(axis='y', ls='--', c='#111', alpha=0.3)

    # Plot average pace
    ax.axhline(y=avg_speed, color='#ff2179', linestyle='--', label='Average pace')

    # Some useful text
    ax.legend()
    total_time = pace_df.time.sum()
    minutes, sec = divmod(total_time, 60)
    ax.set_title('{date} run: {km}km in {min}:{sec:02d}'.format(
        date=point.time.strftime('%b %d'), km=round(total_dist / 1000, 2), min=round(minutes), sec=round(sec)
    ))
    fig.set_tight_layout(True)

    return fig


if __name__ == '__main__':
    with open(sys.argv[1]) as fh:
        splits = [int(x) for x in sys.argv[2].split(',')]
        ax = get_graph(fh, splits)
        plt.show()
