import re
import string

from nltk.stem.snowball import SnowballStemmer

stemmer = SnowballStemmer('russian')
russian_letters = 'абвгдеёжзийклмнопстуфхцчшщъыьэюя'

swear_words = {'говн', 'пиздец', 'говнищ', 'уебищн', 'уебанск'}


def get_swear_documents(data):
    swear_docs = []

    for doc in data:
        # Join comments to one string
        comments_str = ' '.join(doc['a_comments'])
        # Replace non-breakable space with regular space
        comments_str = comments_str.replace('\xa0', ' ')
        # Make lowercase
        comments_str = comments_str.lower()
        # Remove punctuation
        comments_str = re.sub(f'[{re.escape(string.punctuation)}]', '', comments_str)
        russian_words = [word for word in comments_str.split() if all(ch in russian_letters for ch in word)]
        stemmed_words = {stemmer.stem(word) for word in russian_words}

        if any(swear_word in stemmed_words for swear_word in swear_words):
            swear_docs.append(doc)

    return swear_docs
