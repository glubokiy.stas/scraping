#!/bin/bash

if [ "$FLASK_ENV" = "development" ]; then
    echo WORKING IN DEV ENVIRONMENT
    flask run -h 0.0.0.0 -p 8002
else
    echo WORKING IN PROD ENVIRONMENT
    gunicorn --bind 0.0.0.0:8002 server:app
fi
