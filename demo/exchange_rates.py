import logging
import os
import time
from datetime import datetime

import requests
import telebot
from parsel import Selector

from mongo import mongo

logger = logging.getLogger("exchange-rates-tracker")

bot = telebot.TeleBot(os.getenv('BOT_TOKEN'))


def get_user(username=os.getenv('EXCHANGE_USER')):
    col = mongo.scraping.users
    user = col.find_one({'username': username})
    if user is None:
        col.insert_one({
            'username': username,
            'notifications_enabled': True,
            'max_margin_pct': 0.9,
        })
        user = col.find_one({'username': username})
    return user


def prior_login(s: requests.Session):
    url = "https://www.ibank.priorbank.by/Bia.Portlets.Ib.Prior.Membership.Login/Login/Index?prtlId=prtl0&controller=&view=&title=&_={}"
    resp = s.get(url.format(int(time.time() * 1000)))
    sel = Selector(resp.text)

    url = "https://www.ibank.priorbank.by/Bia.Portlets.Ib.Prior.Membership.Login/Login/Index"
    payload = {
        "UserName": os.getenv('PRIOR_USER'),
        "Password": os.getenv('PRIOR_PSWD'),
        "__RequestVerificationToken": sel.css("input[name*=Token]::attr(value)").get(),
    }
    headers = {
        "authority": "www.ibank.priorbank.by",
        "sec-ch-ua": '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        "accept": "*/*",
        "x-requested-with": "XMLHttpRequest",
        "sec-ch-ua-mobile": "?0",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "origin": "https://www.ibank.priorbank.by",
        "sec-fetch-site": "same-origin",
        "sec-fetch-mode": "cors",
        "sec-fetch-dest": "empty",
        "referer": "https://www.ibank.priorbank.by/",
        "accept-language": "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7",
    }
    s.post(url, data=payload, headers=headers)


def get_business_rate():
    s = requests.Session()
    prior_login(s)

    resp = s.post(
        "https://www.ibank.priorbank.by/Bia.Portlets.Ib.Prior.FxTrade2020/FxTrade2020/UpdateCurrencyPairWidget",
        json={
            "CurrencyPairsCodes": "USDBYN",
            "Amount": "100",
            "SelectedPairNumber": "840",
            "SelectedPairAlfaCode": "BYN",
            "SecondPairAlfaCode": "USD",
            "SelectedPaymentDateCode": "TOD",
            "IsUpdated": True,
            "WidgetId": "5af0be52-23e7-492e-8295-e5a195da0cc8",
            "IsWidgetLocked": False,
            "IsEnabled": True,
        },
    )
    data = resp.json()
    return data["UpdatedWidget"]["SellingRate"]


def get_private_rate():
    s = requests.Session()
    response = s.get('https://www.priorbank.by/offers/services/currency-exchange')
    sel = Selector(response.text)
    code = sel.css('script:contains(getRateChanges)').re_first('INSTANCE_([0-9A-Za-z]+)')
    url = f'https://www.priorbank.by/offers/services/currency-exchange?p_p_id=ExchangeRates_INSTANCE_{code}&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=ajaxPeriodPageRatesGetRates&p_p_cacheability=cacheLevelPage'
    data = {
        f'_ExchangeRates_INSTANCE_{code}_channels': 3,
        f'_ExchangeRates_INSTANCE_{code}_codesType': 'checkedCodes',
        f'_ExchangeRates_INSTANCE_{code}_currencyCodes': 840,
        f'_ExchangeRates_INSTANCE_{code}_dateFrom': datetime.utcnow().strftime('%d.%m.%Y'),
        f'_ExchangeRates_INSTANCE_{code}_dateTo': datetime.utcnow().strftime('%d.%m.%Y'),
        f'_ExchangeRates_INSTANCE_{code}_timestamp': int(datetime.utcnow().timestamp() * 1000),
    }
    resp = s.post(url, data=data)
    sel = Selector(resp.json()['currencyHTML'])
    rate = sel.css('.exch-rates__table td:contains("Доллар")').xpath('./following-sibling::td[text()][2]/text()').get()
    return float(rate)


def update_and_notify():
    try:
        business, private = get_business_rate(), get_private_rate()
    except Exception:
        logger.exception("Something went wrong")
        return False

    margin_pct = (private - business) / private * 100
    msg = f"Маржа сейчас: {margin_pct:.2f}%\nПриор ИП: {business:.4f}\nПриор физ: {private:.4f}"
    logger.info(msg)

    mongo.scraping.exchange_rates.insert_one({
        'private': private,
        'business': business,
        'at': datetime.utcnow()
    })

    user = get_user()

    # Only notify about low margins
    if margin_pct < user['max_margin_pct'] and user['notifications_enabled']:
        bot.send_message(319489472, text=msg)

    return True


def main():
    logging.basicConfig(
        level="INFO",
        format="%(asctime)s [%(name)s] %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    while True:
        is_ok = update_and_notify()
        if is_ok:
            time.sleep(10 * 60)
        else:
            time.sleep(5 * 60)


if __name__ == "__main__":
    main()
