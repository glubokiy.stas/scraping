## Tests

To run tests, use the following command:

```
$ python -W ignore -m unittest discover scrapers/tests
```
