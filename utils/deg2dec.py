import re


def deg2dec(s):
    """Convert degree formatted coordinate (36°45′14″N) to decimal format (36.753889)."""
    degrees, mins, secs, direction = re.match(r'([\d.]+)°(?:([\d.]+)′)?(?:([\d.]+)″)?(\w)', s).groups()
    decimal_value = float(degrees) + float(mins or 0) / 60 + float(secs or 0) / 3600
    if direction in ['S', 'W']:
        decimal_value *= -1

    return round(decimal_value, 6)
