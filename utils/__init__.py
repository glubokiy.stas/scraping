from utils.deg2dec import deg2dec
from utils.get_class_from_method import get_class_from_method
from utils.join_selected import join_selected
from utils.replace_nbsp import replace_nbsp
