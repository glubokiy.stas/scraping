def join_selected(elem, sel):
    return ''.join(x.strip() for x in elem.css(sel).getall())
