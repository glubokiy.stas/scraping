def replace_nbsp(s):
    if isinstance(s, str):
        return s.replace('\xa0', ' ')

    if isinstance(s, list):
        return [x.replace('\xa0', ' ') for x in s]

    return s
