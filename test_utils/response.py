from scrapy import Request
from scrapy.http import HtmlResponse


class LocalResponse:

    def __new__(cls, file_path, url='http://www.example.com'):
        request = Request(url=url)

        with open(file_path) as response_file:
            response = HtmlResponse(
                url=url,
                request=request,
                body=response_file.read(),
                encoding='utf-8'
            )

        return response
