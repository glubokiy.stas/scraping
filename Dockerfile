FROM python:3.6

WORKDIR /usr/src/app

# Install requirements
COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt

# Copy source code
COPY . .

CMD ["scrapy", "crawl"]
